{
	// User
	"_id":"john01",
	"firstName": "John",
	"lastName" : "Smith",
	"email" : "johnsmith@mail.com",
	"password" : "johnsmith123",
	"isAdmin" : true,
	"mobileNumber" : "09123456789",
}

{
	//Orders
 	"user Id": "john01",
    "transactionDate": "06-10-2022",
    "status" :"Paid",
    "total" : "200",
}

{
	//Order Products	
	"order ID" :"0001",
    "product ID" : "P-0001",
    "quantity"	: "2",
    "price" : "200",
    "sub Total" : "200",
}


{
	//Products
	"name" : "White T-shirt"
    "description" : "Cotton White T-shirt"
    "price" : "200",
    "stocks" : "500",
    "isActive": true,
    "SKU" : "KS93528TUT",
}